package top.lyc829.iot_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.lyc829.iot_server.bean.User;

public interface UserDao extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    User findUserById(Integer userId);
    User findUserByNfcId(String nfcId);
    User findUserByPassword(String password);
}
