package top.lyc829.iot_server.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.bean.UserFinger;
import top.lyc829.iot_server.dao.UserDao;
import top.lyc829.iot_server.dao.UserFingerDao;

@Service
public class UserContainer {

    private User user;
    private String fingerName;

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserFingerDao userFingerDao;

    public void setUser(User user){
        this.user = user;
    }

    public void setFingerName(String fingerName){
        this.fingerName = fingerName;
    }

    public void saveFingerId(Integer fingerId){
        if (user == null) return;
        UserFinger userFinger = new UserFinger();
        userFinger.setFingerId(fingerId);
        userFinger.setUserId(user.getId());
        userFinger.setFingerName(fingerName);
        userFingerDao.save(userFinger);
    }

    public String getName(){
        if (user == null){
            return "";
        }
        return user.getName();
    }

    public void saveNfcId(String nfcId){
        if (user == null) return;
        user.setNfcId(nfcId);
        userDao.save(user);
    }
}
