package top.lyc829.iot_server;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import top.lyc829.iot_server.utils.MqttUtil;

import java.util.Scanner;

public class MqttInstance {

    public static void main(String[] args) {
        MqttUtil mqttUtil = MqttUtil.build("tcp://192.168.0.109:1883","IOT_SERVER_instance");

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    System.out.print("topic: ");
                    String topic = new Scanner(System.in).next();
                    System.out.print("content: ");
                    String content = new Scanner(System.in).next();
                    mqttUtil.publish(topic,content);
                }
            }
        }).start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true){
//                    mqttUtil.publish("/client/add_finger","11");
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                mqttUtil.subscribe("/server/openDoorFail", new MqttUtil.Callable() {
//                    @Override
//                    public void callback(MqttMessage mqttMessage) {
//                        System.out.println("fail: " + mqttMessage);
//                    }
//                });
//
//                mqttUtil.subscribe("/server/openDoorSuccess", new MqttUtil.Callable() {
//                    @Override
//                    public void callback(MqttMessage mqttMessage) {
//                        System.out.println("success: " + mqttMessage);
//                    }
//                });
//            }
//        }).start();
    }

}
