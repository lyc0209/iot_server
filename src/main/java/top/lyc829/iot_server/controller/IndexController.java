package top.lyc829.iot_server.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import top.lyc829.iot_server.bean.AccessLog;
import top.lyc829.iot_server.service.AccessLogService;
import top.lyc829.iot_server.service.UserService;
import top.lyc829.iot_server.utils.Result;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private AccessLogService accessLogService;

    @Autowired
    private UserService userService;

    @GetMapping("/get_logs")
    public Result getActivityInfo(@RequestParam(value = "page") int page) {
        return Result.success(accessLogService.findAccessLogs(page));
    }

    @PostMapping("/open")
    public Result openDoor(@RequestBody Integer id) {
        return userService.openDoor(id);
    }

}
