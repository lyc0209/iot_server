package top.lyc829.iot_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import top.lyc829.iot_server.bean.AccessLog;
import top.lyc829.iot_server.bean.User;

import java.util.List;

public interface AccessLogDao extends JpaRepository<AccessLog, Integer>, JpaSpecificationExecutor<AccessLog> {

}
