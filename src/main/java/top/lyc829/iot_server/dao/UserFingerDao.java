package top.lyc829.iot_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.bean.UserFinger;

import java.util.List;

public interface UserFingerDao extends JpaRepository<UserFinger,Integer> {

    @Query(value = "select u from UserFinger u where u.fingerId = :fingerId")
    UserFinger findByFingerId(Integer fingerId);

    List<UserFinger> findUserFingersByUserId(Integer userId);

    @Query(value = "select max(finger_id) from user_finger",nativeQuery = true)
    Integer getMax();
}
