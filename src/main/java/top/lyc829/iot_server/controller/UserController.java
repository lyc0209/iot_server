package top.lyc829.iot_server.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.bean.UserFinger;
import top.lyc829.iot_server.service.FingerService;
import top.lyc829.iot_server.service.NfcService;
import top.lyc829.iot_server.service.UserService;
import top.lyc829.iot_server.utils.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    FingerService fingerService;

    @Autowired
    NfcService nfcService;

    @PostMapping("/login")
    public Result login(@RequestBody User user, HttpServletResponse response) throws InterruptedException {
        return userService.doLogin(user, response);
    }

    /**
     * 获取室友列表
     * @return
     */
    @GetMapping("/friends")
    public Result getFiends() {
        return userService.getFriends();
    }

    /**
     * 获取指纹列表
     * @param userId 用户id
     * @return
     */
    @GetMapping("/fingers")
    public Result getFingers(@RequestParam(value = "id") int userId) {
        return fingerService.getFingers(userId);
    }

    /**
     * 删除指纹
     * @param finger 指纹
     * @return
     */
    @PostMapping("/fingers/delete")
    public Result deleteFinger(@RequestBody UserFinger finger) {
        return fingerService.deleteFingerById(finger.getId());
    }

    @PostMapping("/fingers/add")
    public Result addFinger(@RequestBody UserFinger finger) {
        return fingerService.addFinger(finger);
    }

    /**
     * 更新指纹名称
     * @param finger 指纹
     * @return
     */
    @PostMapping("/fingers/update")
    public Result updateFinger(@RequestBody UserFinger finger) {
        return fingerService.updateFinger(finger);
    }

    /**
     * 获取卡片
     * @param userId 用户id
     * @return
     */
    @GetMapping("/nfcs")
    public Result getNfc(@RequestParam(value = "id") int userId) {
        return nfcService.getNfc(userId);
    }

    /**
     * 删除卡片
     * @param user 用户
     * @return
     */
    @PostMapping("/nfcs/delete")
    public Result deleteNfc(@RequestBody User user) {
        return nfcService.deleteNfc(user);
    }

    @PostMapping("/nfcs/add")
    public Result addNfc(@RequestBody Integer id) {
        return nfcService.addNfc(id);
    }

}
