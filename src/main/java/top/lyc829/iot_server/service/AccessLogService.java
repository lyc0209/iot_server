package top.lyc829.iot_server.service;

import top.lyc829.iot_server.bean.AccessLog;

import java.util.List;

public interface AccessLogService {

    List<AccessLog> findAccessLogs(int page);

    /*
     * 添加记录
     * 用户
     * 开门种类
     * 开门凭证
     * 开门是否成功
     */
    void add(Integer userId,
             Integer type,
             String typeId,
             Integer state);

}
