package top.lyc829.iot_server.service;

import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.utils.Result;

public interface NfcService {

    /*
     * 开门
     * 返回开门用户的姓名
     */
    String open(String id);

    Result getNfc(int userId);

    Result deleteNfc(User user);

    Result addNfc(Integer id);
}
