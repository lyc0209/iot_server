package top.lyc829.iot_server;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import top.lyc829.iot_server.utils.MqttUtil;

import java.util.Arrays;

@SpringBootApplication
public class IotServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IotServerApplication.class, args);
    }

}
