package top.lyc829.iot_server.bean;

import lombok.Data;
import org.omg.CORBA.PRIVATE_MEMBER;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_finger")
public class UserFinger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;

    private Integer fingerId;

    // 别名
    private String fingerName;

    public String getFingerName() {
        return fingerName;
    }

    public void setFingerName(String fingerName) {
        this.fingerName = fingerName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFingerId() {
        return fingerId;
    }

    public void setFingerId(Integer fingerId) {
        this.fingerId = fingerId;
    }


}
