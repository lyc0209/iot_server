package top.lyc829.iot_server.service;

import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.utils.Result;

import javax.servlet.http.HttpServletResponse;

public interface UserService {

    User findUserById(Integer userId);
    User findUserByNfcId(String nfcId);
    User findUserByPassword(String password);

    void updateUser(User user);

    Result doLogin(User user, HttpServletResponse response);

    Result getFriends();

    Result openDoor(Integer id);
}
