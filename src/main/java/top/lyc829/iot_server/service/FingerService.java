package top.lyc829.iot_server.service;

import org.springframework.data.jpa.repository.Query;
import top.lyc829.iot_server.bean.UserFinger;
import top.lyc829.iot_server.utils.Result;

import java.util.List;

public interface FingerService {


    String open(String content);


    Result getFingers(int id);

    Result deleteFingerById(int fingerId);

    Result updateFinger(UserFinger finger);

    Result addFinger(UserFinger finger);
}
