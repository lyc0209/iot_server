package top.lyc829.iot_server;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.lyc829.iot_server.bean.User;
import top.lyc829.iot_server.dao.UserDao;
import top.lyc829.iot_server.service.UserService;
import top.lyc829.iot_server.service.impl.UserServiceImpl;

@SpringBootTest
class IotServerApplicationTests {

    @Test
    void contextLoads() {
    }

}
